 
> `lianxh` 命令：在 Stata 里看推文



## 1. 简介

`lianxh` 命令是连享会编写的一个小程序，目的在于让用户可以便捷地从 Stata 窗口中使用关键词检索 [连享会](https://www.lianxh.cn) 发布的推文，同时，也可以列出常用的 Stata 资源链接，包括 Stata 官网地址，Stata 官方 [FAQs](https://www.stata.com/support/faqs/)，Stata 论坛 ([Statalist](https://www.statalist.org/forums/))，[Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html)；Stata 网络教程、[论文重现资料](https://www.lianxh.cn/news/e87e5976686d5.html) 等。

## 2. 安装

目前，你可以通过 [码云仓库](https://arlionn.gitee.io/lianxh) 来安装 `lianxh` 命令。待程序测试稳定后，将通过 SSC 发布，届时大家便可以通过 `ssc install lianxh` 来安装了。

在 Stata 命令窗口中输入如下命令即可安装：

```stata
. net install lianxh.pkg, from(https://arlionn.gitee.io/lianxh) replace 
```
查看帮助文件：
```stata
. help lianxh
```
## 3. 用法


### 常用命令
```stata
. lianxh        // 呈现 Stata 资源链接和常用网址
 
. lianxh all    // 呈现所有分类

. lianxh DID    // 呈现包含 DID 关键词的推文

. lianxh DID 倍分 双差分 // 多个关键词查询 (并集)

. lianxh DID+倍分        // 多个关键词查询 (交集)

. lianxh 资源

. lianxh book

. lianxh sj

. lianxh DID, m   // Markdown 爱好者的惊喜
```

### 示例
```stata
. lianxh        // 呈现 Stata 资源链接和常用网址
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201109172713.png)


```stata
. lianxh all 
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

```stata
. lianxh DID
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201109174309.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

```stata
. lianxh DID 倍分 双差分
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201109174409.png)

```stata
. lianxh DID+倍分
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201109174455.png)



## 4. 宝贵建议

`lianxh` 还不完善，大家使用过程中若发现 bugs，或有好的建议，均可发邮件至 <StataChina@163.com>。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 相关课程

&emsp;

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟.    
> - [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  


&emsp;

---
### 课程一览   


> 支持回看

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)**|   | 因果推断, 空间计量，寒暑假班等  |
| &#x2B55; **[数据清洗系列](https://gitee.com/arlionn/dataclean)** | 游万海| 直播, 88 元，已上线 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |

> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

